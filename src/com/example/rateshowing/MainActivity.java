package com.example.rateshowing;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

//import com.example.exchangerate.ExchangeCalActivity;
//import com.example.exchangerate.MainActivity;
//import com.example.exchangerate.R;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {
	final String TAG = "EXCHANGE";
String currency;
double content;
Reader reader = null;
double rate;




	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Button b1 = (Button)findViewById(R.id.button1);
		b1.setOnClickListener(this);
		Button b2 = (Button)findViewById(R.id.button2); 
		b2.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void onClick(View v) {
		int id = v.getId();
		
	
			
		switch(id){
		
		//If press THB to xxx button
			case R.id.button1:
				EditText et1 = (EditText)findViewById(R.id.input1);
				TextView output1 = (TextView)findViewById(R.id.output1);
				double num1 = Double.parseDouble(et1.getText().toString().trim());
				double ans1 = num1 * rate;
				
				if(num1!=0){
					output1.setText(String.format("%.3f",ans1));
				}else if(num1==0){
					output1.setText(new StringBuilder("0"));
				}
				break;
			//If press xxx to THB button
			case R.id.button2:
				EditText et2 = (EditText)findViewById(R.id.input2);				
				TextView output2 = (TextView)findViewById(R.id.output2);
				Double num2 = Double.parseDouble(et2.getText().toString().trim());
				Double ans2 = num2 / rate;
				
				
				if(num2!=0){
					output2.setText(String.format("%.3f",ans2));
				}else if(num2==0){
					output2.setText(new StringBuilder("0"));
				}
				break;
		} 
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		
		TextView title = (TextView)findViewById(R.id.title);
		TextView currency1 = (TextView)findViewById(R.id.currency);
		TextView currency2 = (TextView)findViewById(R.id.currency2);
		TextView currency3 = (TextView)findViewById(R.id.currency3);
		TextView rate1 = (TextView)findViewById(R.id.rate);
	
		
		ConnectivityManager mgr = (ConnectivityManager)
				getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = mgr.getActiveNetworkInfo();
		if (info != null && info.isConnected()) {
			
			
				LoadRateData task = new LoadRateData(this);
				Log.d("EXCHANGE", "execute task");
				task.execute("http://rate-exchange.appspot.com/currency?" + 
        			"from=THB&to=USD");
				
				title.setText("USD");
				currency1.setText("USD");
				currency2.setText("USD");
				currency3.setText("Input USD");
				rate1.setText(Double.toString(rate));
				
				
			}
		
		else {
			Toast t = Toast.makeText(this, 
					"No Internet Connectivity on this device. Check your setting", 
					Toast.LENGTH_LONG);
			t.show();
		}
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		TextView title = (TextView)findViewById(R.id.title);
		TextView currency1 = (TextView)findViewById(R.id.currency);
		TextView currency2 = (TextView)findViewById(R.id.currency2);
		TextView currency3 = (TextView)findViewById(R.id.currency3);
		TextView rate1 = (TextView)findViewById(R.id.rate);
		EditText et1 = (EditText)findViewById(R.id.input1);
		EditText et2 = (EditText)findViewById(R.id.input2);	
		TextView output1 = (TextView)findViewById(R.id.output1);
		TextView output2 = (TextView)findViewById(R.id.output2);
		ImageView image = (ImageView) findViewById(R.id.imageView1);
		switch (id) {
		case R.id.USD:
			LoadRateData task = new LoadRateData(this);
			Log.d("EXCHANGE", "execute task");
			task.execute("http://rate-exchange.appspot.com/currency?" + 
    			"from=THB&to=USD");
			currency = "USD";
			
			title.setText(currency);
			currency1.setText(currency);
			currency2.setText(currency);
			currency3.setText("Input "+ currency);
			et1.setText("");
			et2.setText("");
			output1.setText("0.00");
			output2.setText("0.00");
			
			image.setImageResource(R.drawable.usd);
		
			break;
		case R.id.GBP:
			LoadRateData task1 = new LoadRateData(this);
			Log.d("EXCHANGE", "execute task");
			task1.execute("http://rate-exchange.appspot.com/currency?" + 
    			"from=THB&to=GBP");
			currency = "GBP";
		
			title.setText(currency);
			currency1.setText(currency);
			currency2.setText(currency);
			currency3.setText("Input "+ currency);
			et1.setText("");
			et2.setText("");
			output1.setText("0.00");
			output2.setText("0.00");
			image.setImageResource(R.drawable.gbp);
			break;
			
		case R.id.EUR:
			LoadRateData task6 = new LoadRateData(this);
			Log.d("EXCHANGE", "execute task");
			task6.execute("http://rate-exchange.appspot.com/currency?" + 
    			"from=THB&to=EUR");
			currency = "EUR";
			
			title.setText(currency);
			currency1.setText(currency);
			currency2.setText(currency);
			currency3.setText("Input "+ currency);
			et1.setText("");
			et2.setText("");
			output1.setText("0.00");
			output2.setText("0.00");
			image.setImageResource(R.drawable.eur);
			break;
			
		case R.id.JPY:
			LoadRateData task2 = new LoadRateData(this);
			Log.d("EXCHANGE", "execute task");
			task2.execute("http://rate-exchange.appspot.com/currency?" + 
    			"from=THB&to=JPY");
			currency = "JPY";
			
			title.setText(currency);
			currency1.setText(currency);
			currency2.setText(currency);
			currency3.setText("Input "+ currency);
			et1.setText("");
			et2.setText("");
			output1.setText("0.00");
			output2.setText("0.00");
			image.setImageResource(R.drawable.jpy);
			break;
			
		case R.id.HKD:
			LoadRateData task3 = new LoadRateData(this);
			Log.d("EXCHANGE", "execute task");
			task3.execute("http://rate-exchange.appspot.com/currency?" + 
    			"from=THB&to=HKD");
			currency = "HKD";
		
			title.setText(currency);
			currency1.setText(currency);
			currency2.setText(currency);
			currency3.setText("Input "+ currency);
			et1.setText("");
			et2.setText("");
			output1.setText("0.00");
			output2.setText("0.00");
			image.setImageResource(R.drawable.hkd);
			break;
			
		case R.id.MYR:
			LoadRateData task4 = new LoadRateData(this);
			Log.d("EXCHANGE", "execute task");
			task4.execute("http://rate-exchange.appspot.com/currency?" + 
    			"from=THB&to=MYR");
			currency = "MYR";
			
			title.setText(currency);
			currency1.setText(currency);
			currency2.setText(currency);
			currency3.setText("Input "+ currency);
			et1.setText("");
			et2.setText("");
			output1.setText("0.00");
			output2.setText("0.00");
			image.setImageResource(R.drawable.myr);
			break;
			
		case R.id.SGD:
			LoadRateData task5 = new LoadRateData(this);
			Log.d("EXCHANGE", "execute task");
			task5.execute("http://rate-exchange.appspot.com/currency?" + 
    			"from=THB&to=SGD");
			currency = "SGD";
			
			title.setText(currency);
			currency1.setText(currency);
			currency2.setText(currency);
			currency3.setText("Input "+ currency);
			et1.setText("");
			et2.setText("");
			output1.setText("0.00");
			output2.setText("0.00");
			image.setImageResource(R.drawable.sgd);
			break;
		
		default:
			return super.onOptionsItemSelected(item);

		}
		return true;
	}
	public Boolean loaddata(String currency){
		boolean result = true;
		URL url;
        StringBuilder data = new StringBuilder();
        int len = 500;
        char[] buffer = new char[len];
        
        //fetch the data from web.
		try {
        	url = new URL("http://rate-exchange.appspot.com/currency?" + 
        			"from=THB&to=" + currency);
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            Log.e("Response", ""+response);
            //Response == 200 -> OK
            //Response == 404 -> Not Found
            if (response == 200) {
            	//reader reads data from the server
                reader = new InputStreamReader(conn.getInputStream(),"UTF-8");
                int count;
                do {
                    count = reader.read(buffer);
                    data.append(buffer);
                } while (count > 0);
                //all the contents are put into a string
                //content = data.toString();
                content = 0;
                try{
                	//Get root of the JSON format
                	JSONObject root = new JSONObject(data.toString());
                		 rate = root.getDouble("rate"); //Get Rate in double
                		content += rate; //Output by use content
                	
                		
                		
                		
        		    	
        		    	
        		    	
        		    	
                }catch(JSONException e){
                	Log.e("JSON", e.toString());
                }
            }
                
        } catch (MalformedURLException mue) {
            Log.e("GetData MalformedURLException", mue.toString());
            result = false;
           
        } catch (IOException ioe) {
            Log.e("GetData IOException", ioe.toString());
            result = false;
           
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    result = false;
                    Log.e("GetData IOException 2", e.getMessage());
                   
                }
            }
        }
            
        //if result == true then we completely got the data
        //textView1.
        return result;
	}

	private class LoadRateData extends AsyncTask<String, Void, String> {
		ProgressDialog dialog;
		public double content;
		Reader reader = null;

		// This method is executed before the background task
		// starts. We use it to set up and display a progress
		// dialog.
		public LoadRateData(Activity m){
			dialog  =new ProgressDialog(m);
		}
		// user can't do anything.
		@Override
		protected void onPreExecute() {
			//ProgressDialog.
			super.onPreExecute();
			dialog.setMessage("Downloading rate data...");
//			dialog.setIndeterminate(true);
//			dialog.setCancelable(true);
			dialog.show();
			
		}

		// This method is run separately on a new thread.
		// Here, we use HttpURLConnection to load the data
		// and store it in a StringBuilder.
		// The data will be loaded only when the HTTP response
		// is 200.
		@Override
    protected String doInBackground(String... params) {
       
        boolean result = true;
		URL url;
        StringBuilder data = new StringBuilder();
        int len = 500;
        char[] buffer = new char[len];
        
        //fetch the data from web.
		try {
        	url = new URL(params[0]);
        	Log.d(TAG,"url:"+url);
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            Log.d(TAG,"Connecting");
            conn.connect();
            
            int response = conn.getResponseCode();
            Log.d(TAG, ""+response);
            //Response == 200 -> OK
            //Response == 404 -> Not Found
            if (response == 200) {
            	//reader reads data from the server
                reader = new InputStreamReader(conn.getInputStream(),"UTF-8");
                int count;
                do {
                    count = reader.read(buffer);
                    data.append(buffer);
                } while (count > 0);
                //all the contents are put into a string
                //content = data.toString();
                content = 0;
                try{
                	//Get root of the JSON format
                	JSONObject root = new JSONObject(data.toString());
                		 rate = root.getDouble("rate"); //Get Rate in double
                		content += rate; //Output by use content
                		
                		
                		
                		
        		    	
        		    	
        		    	
        		    	
                }catch(JSONException e){
                	Log.e("JSON", e.toString());
                }
            }
                
		} catch (IOException e) {
			return "Error while reading data from the server";
		}
           
        
            return data.toString();
      
        
    }
	
		// After finish downloading, the obtained data is set to
		// textView1.
		
		
	

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub

			
			super.onPostExecute(result);
			
			if (dialog.isShowing()) {
				dialog.dismiss();
				TextView rate1 = (TextView)findViewById(R.id.rate);
        		rate1.setText(Double.toString(rate));
				
			}
		} 
		
	}
    
  
   
    
  

    
	
}

